from os import path
from bs4 import BeautifulSoup
import pandas as pd
import matplotlib.pyplot as plt


CURR_PATH = path.dirname(path.realpath(__file__))

#url
filename = CURR_PATH + '\\data\\pbe_veicular_2016.html'

#parser html
#r = requests.get(filename)
#data = r.text
#soup = BeautifulSoup(data, 'html.parser')
#Neste caso utilizei open pois o arquivo estava local
with open(filename) as infile:
    soup = BeautifulSoup(infile, 'html.parser')

#Filtra a tabela de interesse
table = soup.find('table', {"class": 'tabelaConsumo _scrolling'})

#Atribui valores da tabela para lista
data = []
rows = table.find_all('tr')[5:] #as cinco primeiras linhas est�o vazias
for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols if ele])

#Converte lista para dataframe
df = pd.DataFrame(data, columns = ["categoria","marca","modelo","motor","versao","transmissao","ar-Cond.","direcao-assistida","combustivel","NMHC","CO","NOx","reducao","etanol_CO2","gasolina_CO2","etanol_Cidade_km_l","etanol_estrada_km_l","gasolina_cidade_km_l","Gasolina_estrada_km_l","consumo_energetico","comparacao_relativa_categoria","comparacao_absoluta_geral","CONPET"])
#Cria um novo dataframe a partir do dataframe inicial
df_csv = df

#seleciona as 10 primeiras linhas
df_csv = df_csv[:10]

#Substitui os valores faltantes que est�o representados por "\" por "NA"
df_csv = df_csv.replace("\\", "NA")

#substitui "," por "." pois no CVS vamos separar os dados com v�rgula
df_csv['motor'] = df_csv['motor'].str.replace(",", ".")
df_csv['versao'] = df_csv['versao'].str.replace(",", ".")

#Defini colunas float
df_csv['NMHC'] = df_csv['NMHC'].str.replace(",", ".").astype(float)
df_csv['CO'] = df_csv['CO'].str.replace(",", ".").astype(float)
df_csv['NOx'] = df_csv['NOx'].str.replace(",", ".").astype(float)
df_csv['etanol_Cidade_km_l'] = df_csv['etanol_Cidade_km_l'].str.replace(",", ".")
df_csv['etanol_estrada_km_l'] = df_csv['etanol_estrada_km_l'].str.replace(",", ".")
df_csv['gasolina_cidade_km_l'] = df_csv['gasolina_cidade_km_l'].str.replace(",", ".").astype(float)
df_csv['Gasolina_estrada_km_l'] = df_csv['Gasolina_estrada_km_l'].str.replace(",", ".").astype(float)
df_csv['consumo_energetico'] = df_csv['consumo_energetico'].str.replace(",", ".").astype(float)


##Grava CSV
df_csv.to_csv(CURR_PATH + '\\data\pbe_veicular_2016.csv', index=False, sep = ',', encoding='utf-8')

##Filtra motores 1.6
#Localiza o modelo mais economico para compactos com motor 1.6 flex

#Filtra motor 1.6
df_16 = df[df['motor'].str.contains("1.6")==True]

#seleciona as colunas de interesse
df_16 = df_16.ix[:, ['categoria','marca','modelo', 'versao', 'motor', 'combustivel' ,'etanol_Cidade_km_l','etanol_estrada_km_l','gasolina_cidade_km_l','Gasolina_estrada_km_l']]

#substitui dados faltantes representados por "\" por "0"
df_16 = df_16.replace("\\", "0")

#Define colunas float
df_16['etanol_Cidade_km_l'] = df_16['etanol_Cidade_km_l'].str.replace(",", ".").astype(float)
df_16['etanol_estrada_km_l'] = df_16['etanol_estrada_km_l'].str.replace(",", ".").astype(float)
df_16['gasolina_cidade_km_l'] = df_16['gasolina_cidade_km_l'].str.replace(",", ".").astype(float)
df_16['Gasolina_estrada_km_l'] = df_16['Gasolina_estrada_km_l'].str.replace(",", ".").astype(float)

#Filtra compactos
df_16 = df_16[df_16['categoria'] == "COMPACTO"]

#Filtra motores flex
df_16 = df_16[df_16['combustivel'] == "F"]

#Carros compactos 1.6 flex mais economicos
print df_16[df_16['gasolina_cidade_km_l'] == df_16['gasolina_cidade_km_l'].max()]


##Motores 1.6
#Media e desvio padrao
print "Etanol"
print "Media cidade: " + str(df_16['etanol_Cidade_km_l'].median())
print "Media estrada: " + str(df_16['etanol_estrada_km_l'].median())
print "Desvio padrao cidade: " + str(df_16['etanol_Cidade_km_l'].std())
print "Desvio padrao estrada: " + str(df_16['etanol_estrada_km_l'].std())
print "Gasolina"
print "Media cidade: " + str(df_16['gasolina_cidade_km_l'].median())
print "Media estrada: " + str(df_16['Gasolina_estrada_km_l'].median())
print "Desvio padrao cidade: " + str(df_16['gasolina_cidade_km_l'].std())
print "Desvio padrao estrada: " + str(df_16['Gasolina_estrada_km_l'].std())


##boxplot

df_plot = df.ix[:, ['marca','modelo', 'versao', 'motor', 'gasolina_cidade_km_l']]

#substitui dados faltantes representados por "\" por "0"
df_plot = df_plot.replace("\\", "0")

#Define colunas float
df_plot = df_plot[df_plot['gasolina_cidade_km_l'].notnull()]
df_plot = df_plot[df_plot['gasolina_cidade_km_l'] > 0]
df_plot['gasolina_cidade_km_l'] = df_plot['gasolina_cidade_km_l'].str.replace(",", ".").astype(float)

#Cria uma nova coluna para obter a tamanho do motor
df_plot['_motor'] = df_plot['motor'].str[0:3]

#Seleciona apenas a motorizacao de interesse
df_plot = df_plot[df_plot['_motor'] <= "1.6"]
df_plot = df_plot[df_plot['_motor'] <> "1.2"]
df_plot = df_plot[df_plot['_motor'] <> "1.3"]

#gr�fico
bp = df_plot.boxplot(by='_motor')
plt.show(bp)
